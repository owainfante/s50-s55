import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap'
import { Navigate } from 'react-router-dom'
import UserContext from '../userContext'
import Swal from 'sweetalert2';

export default function Register(){
	const { user } = useContext(UserContext)

	const [email, setEmail] = useState('');
	const [firstName, setFirstName] = useState("")
	const [lastName, setLastName] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(firstName)
	console.log(lastName)
	console.log(mobileNo)
	console.log(password1);
	console.log(password2);


	function registerUser(event){
		event.preventDefault()

		// For clearing out the form
		setEmail('')
		setPassword1('')
		setPassword2('')

		// Show an alert
		alert("Thank you for registering!")
		fetch("http://localhost:3001/users/checkEmail",{
			method: "POST",
			headers: {
				"Content-type": "application/json"
			},
			body: JSON.stringify({
				email: email,
			})
		})
		.then(res => res.json())
		.then((data) => {

			if(data === true) {
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different email"
				})
			}else {
				fetch("http://localhost:3001/users/register",{
			method: "POST",
			headers: {
				"Content-type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password1
			})
		})
		.then(res => res.json())
		.then((data) => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Successfully Enrolled!",
					icon: "success",
					text: "You have successfully enrolled for this course"
				})

				navigate("/login")
			}else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
			}
		})

	}

	// For form validation, we use the 'useEffect' to track the values of the input fields and run a validation condition everytime there is user input in those fields.
	useEffect(() => {
		// All input fields must not be empty and the password/verify passwords fields must match in values.
		if((email !== '' && password1 !== '' && password2 !== '' && firstName !== "" && lastName !== "" && mobileNo !=="" && mobileNo.length >= 11 ) && (password1 === password2)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password1, password2])

	// MINI ACTIVITY (20 mins.) 7:55PM
	// Conditionally render the register form to check first if the user is logged in. If so, then redirect to the Courses page, but if not then show the register form.
	// Once finished, push to gitlab and link to boodle.
	return(
		(user.id !== null) ?
			<Navigate to="/courses"/>
		: 
		<Row>
			<Col>
				<Form onSubmit={(event) => registerUser(event)}>
					<h1>Register</h1>
					<Form.Group className="mb-3" controlId="formBasicFirstName">
						<Form.Label>First Name</Form.Label>
						<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
					</Form.Group>

					<Form.Group className="mb-3" controlId="formBasicLastName">
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)}/>
					</Form.Group>

			        <Form.Group className="mb-3" controlId="userEmail">
			            <Form.Label>Email address</Form.Label>
			        {/* 2-way Data Binding is when the value of the state reflects on the value of the input field, and vice-versa. The way to implement it is by using an 'onChange' event listener on the input field and updating the value of the state to the current value of the input field. */}
			            <Form.Control 
			                type="email" 
			                placeholder="Enter email" 
			                value={email}
			                onChange={event => setEmail(event.target.value)}
			                required
			            />
			            <Form.Text className="text-muted">
			                We'll never share your email with anyone else.
			            </Form.Text>
			        </Form.Group>

					<Form.Group className="mb-3" controlId="formBasicMobileNumber">
						<Form.Label>Mobile Number</Form.Label>
						<Form.Control type="tel" placeholder="Enter Mobile Number" value={mobileNo} onChange={e => setMobileNo(e.target.value)}/>
					</Form.Group>

			        <Form.Group className="mb-3" controlId="password1">
			            <Form.Label>Password</Form.Label>
			            <Form.Control 
			                type="password" 
			                placeholder="Password"
			                value={password1}
			                onChange={event => setPassword1(event.target.value)} 
			                required
			            />
			        </Form.Group>

			        <Form.Group className="mb-3" controlId="password2">
			            <Form.Label>Verify Password</Form.Label>
			            <Form.Control 
			                type="password" 
			                placeholder="Verify Password" 
			                value={password2}
			                onChange={event => setPassword2(event.target.value)}
			                required
			            />
			        </Form.Group>

			    	{/* Conditional Rendering is when you render elements depending on a specific logical condition */}
			        { isActive ?
			        	<Button variant="primary" type="submit" id="submitBtn">
				        	Submit
				        </Button>
				    	:
				    	<Button disabled variant="danger" type="submit" id="submitBtn">
				        	Submit
				        </Button>
			        }
			    </Form>
			</Col>
		</Row>
	)
}